implementation module examples

import StdString
import StdInt
import StdTuple
import StdFile
import StdOrdList
from StdFunc import const, o

import System.OS

from iTasks._Framework.Generic import class iTask
from iTasks.API.Core.Types import :: Task, generic gEq, generic gDefault, generic JSONDecode, generic JSONEncode, generic gText, generic gVerify, generic gUpdate, generic gEditMeta, generic gEditor, :: VerifiedValue, :: Verification, :: MaskedValue, :: DataPath, :: InteractionMask, :: TaskAttributes, :: DateTime, instance toString DateTime
from Data.Maybe import :: Maybe
from iTasks.API.Extensions.User import class toUserConstraint(..), :: UserConstraint, instance toString UserConstraint, instance toUserConstraint User, instance toString UserConstraint, instance toString User
from Text.JSON import :: JSONNode, generic JSONEncode, generic JSONDecode
from iTasks._Framework.Generic.Visualization import :: TextFormat(..)
from iTasks._Framework.Generic.Interaction import :: VSt, ::USt, :: VisualizationResult,:: EditMeta, :: VerifyOptions
import qualified iTasks.API.Extensions.User as U
from iTasks.API.Common.TaskCombinators import -&&-, >>-
from iTasks.API.Core.SDSs import currentDateTime
from iTasks.API.Extensions.User import currentUser, :: User(..), :: UserTitle, :: Role, :: UserId, :: SessionId, assign, workerAttributes, :: Password, :: Username, workAs, :: Credentials{..}, users
from iTasks._Framework.SDS import :: ReadWriteShared, :: RWShared, :: ReadOnlyShared
import qualified iTasks.API.Core.Tasks as C
from iTasks.API.Core.Tasks import accWorld
from iTasks.API.Extensions.Distributed.Engine import startDistributedEngine
from iTasks.API.Extensions.Distributed.Instance import instanceServer, instanceClient, instanceFilter, instanceClameFilter
from Data.Void import :: Void
from Data.Map import :: Map
from iTasks.API.Extensions.Admin.WorkflowAdmin import workflow, class toWorkflow(..), :: Workflow, publish, :: PublishedTask{..}, :: TaskWrapper(..), manageWorklist, instance toWorkflow (Task a), instance toWorkflow (WorkflowContainer a), instance toWorkflow (a -> Task b), instance toWorkflow (ParamWorkflowContainer a b), :: WorkflowContainer, :: ParamWorkflowContainer
from iTasks._Framework.Engine import :: ServiceFormat(..), :: WebAppOption, startEngineWithOptions, startEngine, class Publishable, :: ServerOptions{..}, DEFAULT_KEEPALIVE_TIME, instance Publishable [PublishedTask]
from Internet.HTTP import :: HTTPRequest(..), :: HTTPUpload, :: HTTPProtocol, :: HTTPMethod
import iTasks.API.Common.TaskCombinators
from iTasks.API.Core.TaskCombinators import :: TaskCont
from iTasks.API.Common.InteractionTasks import enterInformation, :: EnterOption, :: ViewOption, enterChoice, :: ChoiceOption, viewInformation, enterChoiceWithShared, waitForTimer, updateInformationWithShared, updateSharedInformation
from iTasks.API.Core.Types import class descr, :: Time
from iTasks.API.Extensions.Admin.UserAdmin import manageUsers, loginAndManageWorkList
from iTasks.API.Core.SDSs import currentTime
from iTasks.API.Core.SDSs import sharedStore
from iTasks.API.Extensions.Distributed.Task import :: Domain
import iTasks.API.Extensions.Distributed.Task
import iTasks.API.Extensions.Distributed.SDS
from iTasks.API.Extensions.Distributed.Authentication import domainAuthServer, usersOf, remoteAuthenticateUser, startAuthEngine, enterDomain, currentDistributedUser, currentDomain
import libaryutil
from iTasks.UI.Layout import :: NoUserInterface(..), instance tune NoUserInterface
from iTasks.API.Extensions.Device.Features import hasCamera, device, :: DeviceFeatures, manageDeviceFeaturs
from iTasks.API.Extensions.Picture.JPEG import :: JPEGPicture(..)
from iTasks.API.Extensions.Device.Camera import takePicture
import Text
from iTasks.API.Extensions.SVG.SVGlet import overlay, empty, rect, imageUpdate, :: Image, :: Host, :: ImageOffset(..), :: ImageAlign(..), :: UpdateOption, :: Span, :: UpdateOption, :: Conflict, :: TagSource, :: YAlign, :: XAlign, :: TagRef, :: ImageTag
import Graphics.Scalable
import iTasks.API.Extensions.Distributed.InteractionTasks

base 		:== ""
manage		:== base +++ "Manage/"
baseLocal 	:== base +++ "Local/"
baseDistb 	:== base +++ "Distributed/"

myTasks :: Bool -> [Workflow]
myTasks domainServer =
	[ workflow (manage +++ "Device features") "Manage device featurs" manageDeviceFeaturs
	, workflow (baseDistb +++ "Temperature sensor") "Use (remote) temperature sensor." showTemperature
 	, workflow (baseDistb +++ "Ask question") "Ask question to somebody another server." askQuestionDistributed
	, workflow (baseDistb +++ "Start a pol") "Start a pol" myPoll
	, workflow (baseDistb +++ "Text and picture chat") "Distributed chat with picture support." chatTextOrPicture
	, workflow (baseLocal +++ "Text chat")	"Chat text messages local." 		 chatText
	, workflow (baseDistb +++ "Text chat")	"Distributed chat."			 chatTextDistributed
	, workflow (baseLocal +++ "SVG Temperature") "SVG Temperature"			 svgTemperature
	] ++ (if (domainServer) domainServerWork serverWork) 
where
	domainServerWork = 
		[ workflow (manage +++ "Manage users") 	"Manage system users..." manageUsers
		, workflow "Auth server"                "Auth server" (domainAuthServer) 
		, workflow "Task pool server"   	"Task pool server"      hostTaskPoolServer
		]

	serverWork =
		[ workflow "Intermediate task pool"	"Intermediate task pool" intermediateTaskPoolServer 
		, workflow "Task pool client"   	"Task pool client" 	 connectToTaskPoolServer
		]

:: ConnectToTaskPool = { domain :: Domain, port :: Int }

derive class iTask ConnectToTaskPool

hostTaskPoolServer :: Task Void
hostTaskPoolServer
	= getDomain
	>>- \domain -> enterInformation "Task pool port" []
	>>= \port -> (instanceServer port domain -|| instanceFilter (const True) domain)

connectToTaskPoolServer :: Task Void
connectToTaskPoolServer
	= enterInformation "Connect to task pool" []
	>>= \{ConnectToTaskPool|domain=(Domain host),port} -> (instanceClient host port (Domain host)) -|| (instanceFilter (const True) (Domain host))

intermediateTaskPoolServer :: Task Void
intermediateTaskPoolServer
	= enterInformation "Enter YOUR subdomain" []
	>>= \subdomain -> enterInformation "Enter a port for YOUR task pool server" []
	>>= \serverPort -> enterInformation "Connect to (master) task pool" []
	>>= \{ConnectToTaskPool|domain=(Domain host),port} -> ((instanceClient host port (Domain host)) -|| (instanceClameFilter (const True) (Domain host))) -|| instanceServer serverPort subdomain

// --- chat example
createDuoChat :: (User -> Task (Maybe a)) -> Task [a] | iTask a
createDuoChat myChat
   =           get currentUser
   >>= \me ->  enterChoiceWithShared "Chat with" [] users
   >>= \you -> withShared [ ] (duoChat me you myChat)

duoChat :: u u (u -> Task (Maybe a)) (Shared [a]) -> Task [a] | iTask a & toString u & @: u (Task ())
duoChat me you myChat share
	=    (me   @: chatWith me you myChat share)
	-||- (you  @: chatWith you me myChat share)
	>>|   get share

chatWith :: u u (u -> Task (Maybe a)) (Shared [a]) -> Task () | iTask a & toString u
chatWith me you myChat share
	=   viewSharedInformation ("Chat with " +++ (toString you)) [] share
	||- enterChats
where
	enterChats
		=              myChat me
		>>= \mbChat -> case mbChat of
			Nothing  -> return ()
			Just new -> upd (\chats -> chats ++ [new]) share
				    >>| enterChats

:: Msg a =  { name    :: String
            , time    :: Time
            , message :: a
            }

derive class iTask Msg

enterChat :: u -> Task (Maybe (Msg a)) | iTask a & toString u
enterChat me
        =     enterInformation "" []
    >>* [ OnAction (Action "Send" []) (hasValue (newMsg me))
        , OnAction (Action "Stop" []) (always (return Nothing))
        ]

newMsg :: u a -> Task (Maybe (Msg a)) | iTask a & toString u
newMsg me msg
        =             'C'.get currentTime
        >>= \time ->  return (Just { name    = toString me
                                   , time    = time
                                   , message = msg
                             })

chatText :: Task [Msg String]
chatText = createDuoChat enterChat
// --- end chat example

// --- chat distributed example
createDistChat :: (DomainUser -> Task (Maybe a)) -> Task [a] | iTask a
createDistChat myChat
 =                     get currentDistributedUser
 >>= \(me,myDomain) -> enterDomain
 >>= \yourDomain    -> usersOf yourDomain
 >>=                   enterChoice "Chat with" []
 >>= \you           -> withShared [] 
                       (duoChat (me @. myDomain) 
                       (you @. yourDomain) myChat)

chatTextDistributed :: Task [Msg String]
chatTextDistributed = createDistChat enterChat
// --- end distributed example

// --- chat with picture
:: TextPicture = T String
               | P JPEGPicture

derive class iTask TextPicture

enterPictureChat :: u -> Task (Maybe (Msg TextPicture)) | toString u
enterPictureChat me
 =           'C'.get device
 >>= \dev -> enterInformation "" []
 >>* [ OnAction (Action "Send" []) (hasValue (newMsg me o T))
     , OnAction (Action "Picture" []) (ifCond (hasCamera dev) (mkPict me))
     , OnAction (Action "Stop" []) (always (return Nothing))
     ]
where
 mkPict :: u -> Task (Maybe (Msg TextPicture)) | toString u
 mkPict me
   =   takePicture
   >>= maybe (enterChat me) (newMsg me o P)

chatTextOrPicture :: Task [Msg TextPicture]
chatTextOrPicture = createDistChat enterPictureChat
// --- end chat with picture

// --- end chat

// --- pol
pollUsers :: Task [(a, Int)] | iTask a & Eq a & Ord a
pollUsers
 =                enterInformation "Enter poll question" []
 >>= \question -> enterInformation "Enter options" []
 >>= \options  -> enterDomain
 >>= \domain   -> usersOf domain
 >>= \users    -> let task = poll question options
                  in allTasks [timeoutTask timeoutTime
                               (u @. domain @: task) \\ u <- users
                              ]
 >>= \result   -> viewInformation "Pol result" []
                            (sort (countPolls options result))
where
 poll :: String [a] -> Task a | iTask a
 poll question options
  = enterChoice question [] options >>= return

 countPolls :: [a] [Maybe a] -> [(a,Int)] | Eq a
 countPolls options pols = [(op,count op) \\ op <- options]
 where
  count op = length [pol \\ (Just pol) <- pols | op == pol]

 timeoutTime :: Time
 timeoutTime = {Time| hour=0,min=15,sec=0}

timeoutTask :: Time (Task a) -> Task (Maybe a) | iTask a
timeoutTask time task
 =    (waitForTimer time  >>| return Nothing)
 -||- (task >>= return o Just)

myPoll :: Task [(String, Int)]
myPoll = pollUsers
// --- end pol

// --- temperature
:: Temp :== Maybe Real

interval :: Time
interval = { Time | hour = 0, min = 0, sec = 15 }

showTemperature :: Task Temp
showTemperature
 = withShared Nothing showAndTell

showAndTell :: (Shared Temp) -> Task Temp
showAndTell share
 =   updateSharedInformation title [thermometer] share
 -|| (myPi @: forever (readTemp interval Nothing share))
where
 title = "Temperature is:"
 myPi = (Requires "RaspberryPiWithTemperatureSensor")

readTemp :: Time (Maybe Real) (Shared Temp) -> Task Temp
readTemp interval oldTemp shareTemp
 =            waitForTimer interval
 >>|          readTempFileAndConvert "TemperatureSensorFile"
 >>= \temp -> if (temp <> oldTemp)
                       (set temp shareTemp)
                       (return temp)

// helper method
readTempFileAndConvert :: String -> Task Temp
readTempFileAndConvert configKey
 = readConfig configKey
 >>- \file -> accWorld (accFiles (maybe (\x -> (Nothing, x)) readFileContent file))
 >>- return o getTemperature
where
 readFileContent :: String *Files -> (Maybe String, *Files)
 readFileContent tempfile files
  # (ok, file, files) = fopen tempfile FReadData files
  | not ok = (Nothing, files)
  # (content, file) = freads file 100
  # (_, files) = fclose file files
  = (Just content, files)

 getTemperature :: (Maybe String) -> Temp
 getTemperature filecontent
  = let content = fromMaybe "" filecontent in
    case (indexOf "\n" content) of
     -1 -> Nothing
     n  -> (Just ((toReal (trim (dropChars (n + 30) content))) / 1000.0))

// SVG

svgTemperature :: Task (Maybe Real)
svgTemperature 
 = viewInformation "SVG Temperature test" [] "" >>| withShared (Just 0.0) (\shared ->
 forever (enterInformation "Enter temperature" [] >>= \val -> set (Just val) shared)
 -|| updateSharedInformation "Temperature is:" [thermometer] shared)

thermometer :: UpdateOption (Maybe Real) (Maybe Real)
thermometer = imageUpdate
                                                (\s -> s)
                                                (\r _ tagSource -> thermometerImage r tagSource)
                                                (\s _ -> s)
                                                (\s _ -> s)
                                                (\_ _ -> Nothing)
                                                (\_ s -> s)

thermometerImage                        :: (Maybe Real) *TagSource -> Image m
thermometerImage value ts
        = overlay [(AtMiddleX, AtMiddleY)] []
           [ term ]
           (Just (empty (px 50.0) (px 146.0)))
where
        fontT                           = normalFontDef "Courier New" 10.0

        tempToHeight value = let height = (fromMaybe 0.0 value) * 2.0
                             in if (height > 104.0) 104.0 height

        term = overlay [(AtMiddleX, AtTop)] []
                   [ kwik ]
                   (Just emptyTerm)

        kwik = above [] []
                        [ (empty (px 12.5) (px (120.0 - (tempToHeight value))))
                        , overlay [(AtMiddleX, AtBottom)] [] [(rect (px 10.5) (px ((tempToHeight value) + 2.0)) <@< { fill = toSVGColor"red" } <@< { stroke = toSVGColor"red" })] (Just (empty (px 12.5) (px ((tempToHeight value) + 2.0))))
                        , (empty (px 12.5) (px 25.0))]
                        (Just (empty (px 12.5) (px 160.0)))

        emptyTerm = overlay [(AtMiddleX, AtBottom)] []
           [ bottom ]
           (Just (overlay [(AtMiddleX, AtBottom)] [] [tube] (Just numbers)))

        tube = overlay [(AtMiddleX,AtTop)] []
                   [ (rect (px 12.5) (px 110.0) <@< { fill = toSVGColor"white" }) ]
           (Just (empty (px 50.0) (px 130.0)))

        numbers
                = above [] []
                        ([empty (px 20.0) (px 10.0)] ++ flatten [ [text fontT (toString (50 - n * 10)), xline Nothing (px 20.0), empty (px 20.0) (px 10.0)] \\ n <- [0..5]])
                        (Just (empty (px 50.0) (px 145.0)))

        bottom
                = overlay [(AtMiddleX,AtMiddleY)] []
                        [ (circle (px 23.0) <@< { fill = toSVGColor"red" } <@< { stroke = toSVGColor"red" }) ]
                        (Just (circle (px 25.0) <@< { fill = toSVGColor"white" }))


// --- temperature

// --- ask question
askQuestionDistributed :: Task String
askQuestionDistributed
	= enterDomain
	>>= \domain -> enterInformation "Enter a question" []
	>>= \question -> ((viewInformation "Waiting for answer" [] "Please wait for somebody to answer your question.")
			  ||- (domain @: (answerQuestion question)))
	>>= \answer -> viewInformation "Answer" [] answer
where
	/*
	 * Answer the question task.
	 * The >>= return is used so that the answer first need to press "continue" to indicate that the answer is stable.
	 */
	answerQuestion :: String -> Task String
	answerQuestion question
		= enterInformation question []
		>>= return
// --- ask question

readConfig :: String -> Task (Maybe String)
readConfig key
	= accWorld (accFiles (readConfig key))
where
	readConfig :: String *Files -> (Maybe String, *Files)
	readConfig key files
		# (ok, file, files) = fopen "./config" FReadData files
		| not ok = (Nothing, files)
		# (value, file) = findKey key file
		# (_, files) = fclose file files
		= (value, files)
	findKey :: String *File -> (Maybe String, *File)
	findKey key file
		# (line, file) = freadline file
		| (trim line) == "" = (Nothing, file)
		= case (startsWith (key +++ "=") (trim line)) of
			True -> (Just (dropChars (textSize key + 1) (trim line)), file)
			False -> findKey key file

Start :: *World -> *World
Start world
	= startEngine 	[ publish "/" (WebApp []) (\_-> startMode (IF_WINDOWS "examples.exe" "examples"))
			] world

:: ServerRole = DomainServer Domain
	      | Server Domain
	      | NoneServer

derive class iTask ServerRole

serverRoleShare :: Shared ServerRole
serverRoleShare = sharedStore "serverRoleShare" NoneServer

getDomain :: Task Domain
getDomain
	= get serverRoleShare
	>>- \info -> case info of
			(DomainServer domain) -> return domain
			(Server domain)	-> return domain

startMode :: String -> Task Void
startMode executable
	= startDistributedEngine executable
	>>| get serverRoleShare 
	>>- \role = case role of
			DomainServer domain -> startAuthEngine domain >>| loginAndManageWorkList "Examples" (myTasks True)
			Server domain -> startAuthEngine domain >>| loginRemote (myTasks False)
			_ -> viewInformation "Welcome" [] "Chose what this iTasks instance is."
		             >>* [ OnAction (Action "Domain server" []) (always (domainServer))
            			 , OnAction (Action "Server" []) (always (server))
            			 ]
where
	server :: Task Void
	server
		= enterDomain 
		>>= \domain -> set (Server domain) serverRoleShare
		>>| startAuthEngine domain >>| loginRemote (myTasks False)

	domainServer :: Task Void
	domainServer
		= enterDomain
		>>= \domain -> set (DomainServer domain) serverRoleShare
		>>| startAuthEngine domain
		>>| loginAndManageWorkList "Examples" (myTasks True)

loginRemote :: ![Workflow] -> Task Void
loginRemote workflows 
	= forever (
		enterInformation "Enter your credentials and login" []
		>>* 	[OnAction (Action "Login" [ActionIcon "login",ActionKey (unmodified KEY_ENTER)]) (hasValue (browseAuthenticated workflows))
				]
	)
where
	browseAuthenticated workflows {Credentials|username,password}
		= remoteAuthenticateUser username password
		>>= \mbUser -> case mbUser of
			Just user 	= workAs user (manageWorklist workflows)
			Nothing		= viewInformation (Title "Login failed") [] "Your username or password is incorrect" >>| return Void

StartAndroid :: !String !String !String !String !String -> Int
StartAndroid appName appPath sdkPath storeOpt libPath
	= if (libaryStart start) 42 0
where
	start :: *World -> *World
	start world
		# options =
			{ appName               = appName
			, appPath               = appPath
			, sdkPath               = if (sdkPath == "") Nothing (Just sdkPath)
			, serverPort            = 8080
			, keepalive             = DEFAULT_KEEPALIVE_TIME
			, webDirPaths           = Nothing
			, storeOpt              = Just storeOpt
			, saplOpt               = Nothing
			}
		= startEngineWithOptions [ publish "/" (WebApp []) (\_-> startMode libPath)
					 ] options world

// Needed for Android to call Start.
foreign export StartAndroid
